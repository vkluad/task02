Task: write a Guess the Number game using the Bash script.\
Subtask: create a random number generator script.\
repo: add src/script\
script: add random number generator\
readme: add the headers of all commits made in this branch\
Subtask completed.\
Subtask: Extract Function refactoring.\
script: add function random_num_gen\
readme: add the headers of all commits made in code_refactoring branch\
Subtask: complated.\
Subtask: implement comparison of randomly generated number X with number Y\
script: add compare the random number with 1st param\
readme: add the headers of all commits made in compare_random_num branch\
Subtask: complated.\
Subtask: User input\
script: add User input\
readme: add the headers of all commits made in user_input branch\
Subtask: complated.\
Subtask: range of numbers\
script: add range of number & user input\
readme: add the headers of all commits made in range_of_number branch\
script: add limitaion\
readme: add the header of previous commit\
Subtask: complated.\
Subtask: guessing attempts\
script: add guessing attempts as 3rd option\
script: add user input\
readme: add the headers of all commit made in guessing_attempts branch\
Subtask: complated.\
Subtask: Code refactoring(add comments for keys line)\
script: code refactoring & add base comments\
readme: add the headers of all commit made in code_refactoring branch\
Subtask: complated.\
Subtask: play-off\
script: add play-off\
readme: add the header of previous and this commits\
Subtask: complated.\
Subtask: Extra\
script: add options\
script: add menu\
script: fix menu\
script: remove number option\
script: fix menu & remove unused code\
script: add logic of 'Game item'\
script: fix settings menu & delete unused code\
script: add function Game\
script: add all other function\
script: add base comments\
readme: add the headers of all commit made in extra branch\
Subtask: complated.\
Task: complated.
